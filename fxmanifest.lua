fx_version 'cerulean'
games { 'gta5' }

author 'Eakarin Mekmontian'
description 'UI resource with vue.js boilerplate'
version '1.1.0'

ui_page 'dist/ui.html'

client_script 'client/index.js'

files {
  'dist/ui.html',
  'dist/index.js',
  'dist/index.css',
  'dist/**',
  'html/vendor/**'
}

dependencies {
  'yarn',
  'webpack'
}

webpack_config 'webpack.config.js'
