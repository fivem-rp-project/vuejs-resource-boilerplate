# vuejs-resource-boilerplate

This repository is used for template of FiveM resource that handle UI with `vue.js`

## Event definition
```javascript
// listen resource event to send 'open' action to NUI
on('resource_name:event_name_open');

// listen resource event to send 'close' action to NUI
on('resource_name:event_name_close');
```

## How to use asset in NUI
- Use as module from `import` or `require` should be relative path
```js
import IconImage from '../vendor/img/icon.png';

...

require('../vendor/img/icon.png');
```
- Use as url eg `url` (in css) or `src` (in html) should be absolute path
```css
@font-face {
  font-family: 'Sukhumvit-medium';
  src: url('vendor/font/SukhumvitSet-Medium.ttf');
}

```
```html
<img :src="'vendor/img/myImage.png'" />
```


**NOTE** all script is written in `javascript` only.

